package mis.pruebas.carritoproductos.controlador;

import mis.pruebas.carritoproductos.modelo.Producto;
import mis.pruebas.carritoproductos.modelo.Proveedor;
import mis.pruebas.carritoproductos.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/almacen/v1/productos/{idProducto}/proveedores")
public class ControladorProveedoresProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto;

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor;

    @GetMapping
    public List<Integer> obtenerProveedoresProducto(@PathVariable long idProducto) {
        return this.servicioProducto.obtenerPorId(idProducto).getIdsProveedores();
    }

    @GetMapping("/{indiceProveedor}")
    public Proveedor obtenerProveedoresProducto(@PathVariable long idProducto,
                                                @PathVariable int indiceProveedor) {
        final long idProveedor = this.servicioProducto
                .obtenerPorId(idProducto)
                .getIdsProveedores().get(indiceProveedor);
        return this.servicioProveedor.obtenerPorId(idProveedor);
    }

    static class ProveedorProducto {
        public int idProveedor;
    }

    @PostMapping
    public void agregarProveedorProducto(@PathVariable long idProducto,
                                         @RequestBody ProveedorProducto prov) {
        try {
            this.servicioProveedor.obtenerPorId(prov.idProveedor);
            this.servicioProducto.obtenerPorId(idProducto)
                    .getIdsProveedores().add(prov.idProveedor);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{indiceProveedorProducto}")
    public void borrarProveedorProducto(@PathVariable long idProducto,
                                        @PathVariable int indiceProveedorProducto) {
        try {
            final List<Integer> listaIds = this.servicioProducto
                    .obtenerPorId(idProducto)
                    .getIdsProveedores();
            try {
                listaIds.remove(indiceProveedorProducto);
            } catch(Exception x) {}
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
